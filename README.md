# Public key

| Key  | Value  |
|------|--------|
| Key ID      | 535AF414 |
| Length      | 4096 |
| Algorithm   | RSA |
| Fingerprint | **F525 F632 8D6C BDE3 2150  4B02 5AD1 7970 535A F414** |

```bash
-----BEGIN PGP PUBLIC KEY BLOCK-----

mQINBFoS760BEACy3iYaxlET+OXPFu7vg1oR6sa/ouJ9BKAct923OtvcMoglC7hQ
tEFehJdmNk2B+UsBPrp8X0lvRrSToKhg5zcq3qMum7SEuvM8MlL3tNKkhBIBDt+h
8qo9y9zkUWrTHsLmC/nqHTE6VSJJAZiS6zrlDYk0CGCnNfCqeB0Ep2NrYjBahVf0
0Y1PHrtzU4zrCIoNxjqBWki6WodC52DyKwr1ixB4QOAuH6wsqvqvo7ly2iSUeQg5
YdC2pdyJesylDkWHC1kGA8sLvI6HGZKpLEfvi1ykjM7AGDv8XbuwF2c/GyEKlXqJ
SpuK0JqSuQcJsnHmNomyJLuulZBhAQbOsipxJzLyU1xqVfs4RFl7lcu8pDt4xZSf
GowkKk7XFhTiUWOjblD49NNS3//yH12FekfdpR93UfGWq2JvHLfcMwd6X2dtoYgi
ELhFxMGKFa3PKvnzaCzNzXC1BPImdgj5aaoKvCq2iihQqKW5TcjUanbMebqpivLC
awADWGeETfq7WZUNMq6bnJw1dFQBI3k/I5s7Nfs27ZBfRooe+xFzwS5ryhG7Lrqv
CtofVyvXXfyC5BmRHK3IKl0ugbqLrgnbi8iVCqj9r2/etkjvSsczI1fHD5SwK4Qs
vPMUmkeCkzmiRbIJ45UXgZ7jfS3o191j38AahStRBcVL2cVNOTpY16L1SwARAQAB
tB1sb2dvdHlwZSA8dmljdG9yQGxvZ290eXBlLnNlPokCVAQTAQgAPhYhBPUl9jKN
bL3jIVBLAlrReXBTWvQUBQJaEu+tAhsDBQkNYmqABQsJCAcCBhUICQoLAgQWAgMB
Ah4BAheAAAoJEFrReXBTWvQUjLAP/03QF6VLah45RSOM2BvMiT50KXjjryCNnW+v
pUdqI600NAWmoAHJ4rrlglO9NXzqxCGHtc57UFMv3R4aeLMQs3tgvzPwLKHpcR4l
RwjDfKTp6ddvAPZInMu8koWU2GZyu/+poe987H84mQGM8rqNT4LvcTqZt39meFXS
jJ8zjsyv8GcuYqK8wyPrGK87PQh1/fRsHaSED0gaPcZZlnO72gAZygwqq0q2U3NK
11XHhICpnzQ4LFcml/5TW/BzWIUz369UfG/hwPgInFhTeelXXTZLtJzezbY4OWXy
ZmRQoHD+EP0/BTqPeyssLSN/VzDOKuuas9xOTpaWGWQOjphP6/odPehYQ2ZTDK+U
JfxyXnKMsXbdhvAPhidpcLvshzW/aIjpweZ7BFRK0NuDT7fAiBi7b9/tEBDqCAI3
7jc7Kabz6tLylYB4alzIWzJoXgq3pELNx6J7yhDKi7/pH4skFBjqIvEaHi/hVIbz
tEH6EvAqVyzZbhLsWBN38vj0Cxoo4uq62fKXPUe6D3KXBZaSpBkLcQEb2cOg9FZ/
E29Kh6oTIYqAQ4iIfRTrbsBhNLMzCluprqi+xfHVua/fruYZr4y4Xan/ovNNEynu
qiFZ7iXCrIC88U+mDGHl4fsVdpiyqTRVKPo93RJJxkM/1GVXH9sgftZlLzTpfiSU
n1ETyXmiuQINBFoS760BEADQy7gOZyiCt/jr7W/WihAooyB7m1nDTg56KIKmqklS
wU9R6QP6G8Wz2tS/PFYLmKwnr1WKstwBC2tGU3ZvwSXb5//BuIKViusptXBIU7j2
TWOzKoR6fKADnJpIkubWPnfiL/Lbve+3g5dq3E8vKWtc4T1AoA0CxDuTRFtnqO49
RFUCPxmX8gEo+igX15kn/3fW86L/oTKVuAaf09Gu0Xu0ob/ThQbRE11YlJ1gg17/
ag4epWBwhk9MXE78jmMoYHJoqxWU8hVH1isEO8b3tzhevI6z19/tM5lYmqbsVsrY
E3yc46SHbHSeQ7ljGRYvHZT9Su2F6LxIOGm0IeBOvIUN70kIzwPpZYBz3eemuYIS
BJ/rFxXc0pOPBp4O3hHwCU+IvHTkIHwYqqVWKZD4fVjXgPsyLsTrK2rX/tnyRRBH
+x93t7R9QNu812q6FLRKdqQYVXNIdVEtrcq+ivZUXM+/tcwcqNOzMaFkxOM8cZNK
yxakIh1KXsBoTLL3cIMea69Jx+xRyCYaDst0LstTeoPy+eKIDOzrUuBRFtVrTAPG
Pcp/Eozo+bCuo+VBz6iGwv7uCybWuo9KNXKVvszDozscEs4I5YIWDwpb4sHNAyVw
CEvhg153oGpn7HQTg2bcfaENTI7gXJmULJ3zA/Yz6REU7f1W2I9mnK7Kf/zx1bc0
4wARAQABiQI8BBgBCAAmFiEE9SX2Mo1sveMhUEsCWtF5cFNa9BQFAloS760CGwwF
CQ1iaoAACgkQWtF5cFNa9BRf6Q//UMmWpcsu8zFPlx2v6fOiwBTry5UukO7z6Nr4
4waPDukQrzal+28vMNNqXZgT9uvHfqmIDxBPkJt73ticR8Kmn243KhRTwC117DoG
dP7jU41Ne+ab5N5FSp66pNIkea0fJEVtLadcz56O2++z713GYqGGFyw2p8yRLbns
4nH+WTtcrsVoLW4ixE3oB6ChywWmOVUL5oSLDmIYwGcQvRQDFXt66fGpkv1a0SlT
bDLEYTebHKSgD6am9tIMQ+BC/ZvOByhAd8Wqgzo2fU4Iw5jRUf6F75c5dmRoyPlN
djYnLHMV5y52iBtHUliRKyBuOJzTgNv4ZY905EIvIjLzehTI2eft4glF+rCq2+Oh
OYhMe4hFpjFt15mSesWik4tWAIrYdD00ytAsA5Ttz8KVZ9zBNyLOovsqz6sRWNfm
tYJB1VAgELQx2u2z8hkogR5PC2mEQe3WDjcQ8vuWwbVVJ0BwKa3Itpb5sDnHTNzB
uiz6PYi0rafRSbPG6AN23BA7rC21UGUipL93jARq5pn1b/F3a33lhnlnZR/hk+7V
5PcqOgbv6UaXOJ3OQuvHeAagmEJOzEO012iS3+4wEu1qFKb6WhJZ/SNfXIbwETut
yRmrVNhN/93lEZVfBRjPLzYCavnkyL/+ksSz8yWFUEsISJ3/VYhQJvIY/cGTvWJu
wTJe/zI=
=J5Qj
-----END PGP PUBLIC KEY BLOCK-----
```

--------------------------
Built with IntelliJ IDEA Open Source License

<img src="https://s3-ap-southeast-1.amazonaws.com/www.logotype.se/assets/logo-text.svg" width="200">

The people at JetBrains supports the Open Source community by offering free licenses. Check out <a href="https://www.jetbrains.com/buy/opensource/">JetBrains Open Source</a> to apply for your project. Thank you!
